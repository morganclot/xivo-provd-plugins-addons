# -*- coding: utf-8 -*-

# Copyright (C) 2010-2014 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import logging
import os.path
import re

from provd import synchronize
from provd.devices.config import RawConfigError
from provd.devices.pgasso import IMPROBABLE_SUPPORT, COMPLETE_SUPPORT, \
    FULL_SUPPORT, BasePgAssociator, UNKNOWN_SUPPORT
from provd.plugins import StandardPlugin, FetchfwPluginHelper, \
    TemplatePluginHelper
from provd.servers.http import HTTPNoListingFileService
from provd.util import norm_mac, format_mac
from twisted.internet import defer, threads

logger = logging.getLogger('plugin.xivo-grandstream')

TZ_NAME = {'Europe/Paris': 'CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00'}
LOCALE = {
    u'de_DE': 'de',
    u'es_ES': 'es',
    u'fr_FR': 'fr',
    u'fr_CA': 'fr',
    u'it_IT': 'it',
    u'nl_NL': 'nl',
    u'en_US': 'en'
}

FUNCKEY_TYPES = {
    u'speeddial': 0,
    u'blf': 1,
    u'park': 9
}

GXP21XX_SERIES = {
    'GXP2130': {'lines': 3, 'accounts': 3},
    'GXP2135': {'lines': 8, 'accounts': 4},
    'GXP2140': {'lines': 4, 'accounts': 4},
    'GXP2160': {'lines': 6, 'accounts': 6},
    'GXP2170': {'lines': 12, 'accounts': 6},
}


class BaseGrandstreamHTTPDeviceInfoExtractor(object):
    # Grandstream Model HW GXP1405 SW 1.0.4.23 DevId 000b8240d55c
    # Grandstream Model HW GXP2200 V2.2A SW 1.0.1.33 DevId 000b82462d97
    # Grandstream Model HW GXV3240 V1.6B SW 1.0.1.27 DevId 000b82632815

    _UA_REGEX = re.compile(r'^Grandstream Model HW (\w+) SW ([^ ]+) DevId ([^ ]+)')

    def extract(self, request, request_type):
        return defer.succeed(self._do_extract(request))

    def _do_extract(self, request):
        ua = request.getHeader('User-Agent')
        if ua:
            return self._extract_from_ua(ua)
        return None

    def _extract_from_ua(self, ua):
        new_ua = re.sub("( V[0-9]\.[0-9][A-Z] )", '', ua)
        m = self._UA_REGEX.match(new_ua)

        if m:
            raw_model, raw_version, raw_mac = m.groups()
            try:
                mac = norm_mac(raw_mac.decode('ascii'))
            except ValueError as e:
                logger.warning('Could not normalize MAC address "%s": %s', raw_mac, e)
            else:
                return {u'vendor': u'Grandstream',
                        u'model': raw_model.decode('ascii'),
                        u'version': raw_version.decode('ascii'),
                        u'mac': mac}
        return None


class BaseGrandstreamPgAssociator(BasePgAssociator):
    def __init__(self, models, version):
        BasePgAssociator.__init__(self)
        self._models = models
        self._version = version

    def _do_associate(self, vendor, model, version):
        if vendor == u'Grandstream':
            if model in self._models:
                if version == self._version:
                    return FULL_SUPPORT
                return COMPLETE_SUPPORT
            return UNKNOWN_SUPPORT
        return IMPROBABLE_SUPPORT


class BaseGrandstreamPlugin(StandardPlugin):
    _ENCODING = 'UTF-8'

    def __init__(self, app, plugin_dir, gen_cfg, spec_cfg):
        StandardPlugin.__init__(self, app, plugin_dir, gen_cfg, spec_cfg)
        # update to use the non-standard tftpboot directory
        self._base_tftpboot_dir = self._tftpboot_dir
        self._tftpboot_dir = os.path.join(self._tftpboot_dir, 'Grandstream')

        self._tpl_helper = TemplatePluginHelper(plugin_dir)

        downloaders = FetchfwPluginHelper.new_downloaders(gen_cfg.get('proxies'))
        fetchfw_helper = FetchfwPluginHelper(plugin_dir, downloaders)
        # update to use the non-standard tftpboot directory
        fetchfw_helper.root_dir = self._tftpboot_dir

        self.services = fetchfw_helper.services()
        self.http_service = HTTPNoListingFileService(self._base_tftpboot_dir)

    http_dev_info_extractor = BaseGrandstreamHTTPDeviceInfoExtractor()

    def _dev_specific_filename(self, device):
        # Return the device specific filename (not pathname) of device
        fmted_mac = format_mac(device[u'mac'], separator='', uppercase=False)
        return 'cfg' + fmted_mac + '.xml'

    def _check_config(self, raw_config):
        if u'http_port' not in raw_config:
            raise RawConfigError('only support configuration via HTTP')

    def _check_device(self, device):
        if u'mac' not in device:
            raise Exception('MAC address needed for device configuration')

    def configure(self, device, raw_config):
        self._check_config(raw_config)
        self._check_device(device)
        self._check_lines_password(raw_config)
        self._add_timezone(raw_config)
        self._add_locale(raw_config)
        model = device.get(u'model')
        if model in GXP21XX_SERIES:
            self._add_fkeys_gxp21xx(raw_config, GXP21XX_SERIES[model])
        else:
            self._add_fkeys(raw_config)
        filename = self._dev_specific_filename(device)
        tpl = self._tpl_helper.get_dev_template(filename, device)

        path = os.path.join(self._tftpboot_dir, filename)
        self._tpl_helper.dump(tpl, raw_config, path, self._ENCODING)

    def deconfigure(self, device):
        self._remove_configuration_file(device)

    def _remove_configuration_file(self, device):
        path = os.path.join(self._tftpboot_dir, self._dev_specific_filename(device))
        try:
            os.remove(path)
        except OSError as e:
            logger.info('error while removing configuration file: %s', e)

    if hasattr(synchronize, 'standard_sip_synchronize'):
        def synchronize(self, device, raw_config):
            return synchronize.standard_sip_synchronize(device)

    else:
        # backward compatibility with older xivo-provd server
        def synchronize(self, device, raw_config):
            try:
                ip = device[u'ip'].encode('ascii')
            except KeyError:
                return defer.fail(Exception('IP address needed for device synchronization'))
            else:
                sync_service = synchronize.get_sync_service()
                if sync_service is None or sync_service.TYPE != 'AsteriskAMI':
                    return defer.fail(Exception('Incompatible sync service: %s' % sync_service))
                else:
                    return threads.deferToThread(sync_service.sip_notify, ip, 'check-sync')

    def get_remote_state_trigger_filename(self, device):
        if u'mac' not in device:
            return None

        return self._dev_specific_filename(device)

    def _check_lines_password(self, raw_config):
        for line in raw_config[u'sip_lines'].values():
            if line[u'password'] == u'autoprov':
                line[u'password'] = u''

    def _add_timezone(self, raw_config):
        if u'timezone' in raw_config and raw_config[u'timezone'] in TZ_NAME:
            raw_config[u'XX_timezone'] = TZ_NAME[raw_config[u'timezone']]
        else:
            raw_config['timezone'] = TZ_NAME['Europe/Paris']

    def _add_locale(self, raw_config):
        locale = raw_config.get(u'locale')
        if locale in LOCALE:
            raw_config[u'XX_locale'] = LOCALE[locale]

    def _add_fkeys(self, raw_config):
        lines = []
        for funckey_no, funckey_dict in sorted(raw_config[u'funckeys'].items()):
            i_funckey_no = int(funckey_no)
            funckey_type = funckey_dict[u'type']
            if funckey_type not in FUNCKEY_TYPES:
                logger.info('Unsupported funckey type: %s', funckey_type)
                continue
            type_code = u'P32%s' % (i_funckey_no + 2)
            lines.append(self._format_line(type_code, FUNCKEY_TYPES[funckey_type]))
            line_code = self._format_code(3 * i_funckey_no - 2)
            lines.append(self._format_line(line_code, int(funckey_dict[u'line']) - 1))
            if u'label' in funckey_dict:
                label_code = self._format_code(3 * i_funckey_no - 1)
                lines.append(self._format_line(label_code, funckey_dict[u'label']))
            value_code = self._format_code(3 * i_funckey_no)
            lines.append(self._format_line(value_code, funckey_dict[u'value']))
        raw_config[u'XX_fkeys'] = u'\n'.join(lines)

    def _add_fkeys_gxp21xx(self, raw_config, model_info):
        lines = []
        line_keys_num = model_info['lines']
        accounts_num = model_info['accounts']
        for i_funckey_no in range(1, 49):
            funckey_no = str(i_funckey_no)
            if i_funckey_no <= 6:
                type_code = u'P13%s' % (2 * i_funckey_no + 61)
                line_code = u'P13%s' % (2 * i_funckey_no + 62)
                label_code = u'P14%s' % (2 * i_funckey_no + 63)
                value_code = u'P14%s' % (2 * i_funckey_no + 64)
            else:
                type_code = u'P23%s' % (4 * i_funckey_no + 772)
                line_code = u'P23%s' % (4 * i_funckey_no + 773)
                label_code = u'P23%s' % (4 * i_funckey_no + 774)
                value_code = u'P23%s' % (4 * i_funckey_no + 775)

            if funckey_no in raw_config[u'funckeys']:
                funckey_dict = raw_config[u'funckeys'][funckey_no]
                funckey_type = funckey_dict[u'type']
                if funckey_type not in FUNCKEY_TYPES:
                    logger.info('Unsupported funckey type: %s', funckey_type)
                    continue
                if i_funckey_no <= line_keys_num:
                    lines.append(self._format_line(type_code, FUNCKEY_TYPES[funckey_type] + 10))
                else:
                    lines.append(self._format_line(type_code, FUNCKEY_TYPES[funckey_type]))
                lines.append(self._format_line(line_code, int(funckey_dict[u'line']) - 1))
                if u'label' in funckey_dict:
                    lines.append(self._format_line(label_code, funckey_dict[u'label']))
                else:
                    lines.append(self._format_line(label_code, ''))
                lines.append(self._format_line(value_code, funckey_dict[u'value']))
            else:
                if i_funckey_no <= accounts_num:
                    lines.append(self._format_line(type_code, 0))
                    lines.append(self._format_line(line_code, i_funckey_no - 1))
                else:
                    lines.append(self._format_line(type_code, -1))
                    lines.append(self._format_line(line_code, 0))
                lines.append(self._format_line(label_code, ''))
                lines.append(self._format_line(value_code, ''))
        raw_config[u'XX_fkeys'] = u'\n'.join(lines)

    def _format_line(self, code, value):
        return u'    <%s>%s</%s>' % (code, value, code)

    def _format_code(self, code):
        if code >= 10:
            str_code = str(code)
        else:
            str_code = u'0%s' % code
        return u'P3%s' % str_code
